<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Manajemen Jenis Obat</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div>
                <a href="<?= base_url('type_obat/add_type') ?>"><button class="btn btn-success">Tambah Data</button></a>
                <table class="table mt-4">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">ID Jenis Obat</th>
                            <th scope="col">Jenis Obat</th>
                            <th scope="col">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 0;
                        foreach ($type as $item) :
                            $no++;
                        ?>
                            <tr>
                                <td><?= $no ?></td>
                                <td><?= $item['id_jenis_obat']; ?></td>
                                <td><?= $item['nama_jenis_obat']; ?></td>
                                <td>
                                    <a href="<?= base_url('type_obat/edit_type/' . $item['id_jenis_obat']) ?>"><button class="btn btn-primary">Edit</button></a>
                                    <a href="<?= base_url('type_obat/delete/' . $item['id_jenis_obat']) ?>" class="btn btn-danger" onclick="return confirm('Are you sure ?')">Delete</a>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
</div>