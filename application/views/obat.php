<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Manajemen Obat</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div>
                <a href="<?= base_url('Obat/add_obat') ?>"><button class="btn btn-success">Tambah Data</button></a>
                <table class="table mt-4">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">ID Obat</th>
                            <th scope="col">Nama Obat</th>
                            <th scope="col">Jenis Obat</th>
                            <th scope="col">Harga</th>
                            <th scope="col">Satuan</th>
                            <th scope="col">stock</th>
                            <th scope="col">Tanggal Expired</th>
                            <th scope="col">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 0;
                        foreach ($Obat as $item) :
                            $no++;
                        ?>
                            <tr>
                                <td><?= $no ?></td>
                                <td><?= $item['id_obat']; ?></td>
                                <td><?= $item['nama_obat']; ?></td>
                                <td><?= $item['id_jenis_obat']; ?></td>
                                <td><?= $item['harga']; ?></td>
                                <td><?= $item['satuan']; ?></td>
                                <td><?= $item['stock']; ?></td>
                                <td><?= $item['tanggal_expired']; ?></td>
                                <td>
                                    <a href="<?= base_url('Obat/edit_obat/' . $item['id_obat']) ?>"><button class="btn btn-primary">Edit</button></a>
                                    <a href="<?= base_url('Obat/delete/' . $item['id_obat']) ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Menghapus Data Ini ?')">Delete</a>

                                </td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                    </tbody>
                </table>

            </div>

        </div>
    </section>
</div>