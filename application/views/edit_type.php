<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Edit Data Obat</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div>
                <form method="post" action="<?= base_url('type_obat/edit_Mdata/') ?>">
                    <input type="hidden" name="id_jenis_obat" value="<?= $Type['id_jenis_obat']; ?>">
                    <div class="mb-3">
                        <label for="username" class="form-label">Nama Jenis Obat</label>
                        <input type="text" class="form-control" id="nama" name="nama_jenis_obat" value="<?= $Type['nama_jenis_obat']; ?>">
                        <button type="submit" class="btn btn-primary mt-4">Simpan</button>
                </form>

            </div>

        </div>
    </section>
</div>