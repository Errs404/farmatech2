<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Tambah Data Jenis Obat</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div>
                <form method="post" action="<?= base_url('Type_obat/insert') ?>">
                    <div class="mb-3">
                        <label for="username" class="form-label">Nama Jenis Obat</label>
                        <input type="text" class="form-control" id="nama" name="nama_jenis_obat">
                        <button type="submit" class="btn btn-primary mt-4">Simpan</button>
                </form>

            </div>

        </div>
    </section>
</div>