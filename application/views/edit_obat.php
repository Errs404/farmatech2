<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Edit Data Obat</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div>
                <form method="post" action="<?= base_url('Obat/edit_Mdata/') ?>">
                    <input type="hidden" name="id_obat" value="<?= $Obat['id_obat']; ?>">
                    <div class="mb-3">
                        <label for="username" class="form-label">Nama Obat</label>
                        <input type="text" class="form-control" id="nama" name="nama_obat" value="<?= $Obat['nama_obat']; ?>">
                        <label for="username" class="form-label">Jenis Obat</label>
                        <input type="text" class="form-control" id="nama" name="id_jenis_obat" value="<?= $Obat['id_jenis_obat']; ?>" disabled readonly>
                        <label for="username" class="form-label">Satuan</label>
                        <input type="text" class="form-control" id="nama" name="satuan" value="<?= $Obat['satuan']; ?>">
                        <label for="username" class="form-label">Harga</label>
                        <input type="text" class="form-control" id="nama" name="harga" value="<?= $Obat['harga']; ?>">
                        <label for="username" class="form-label">Jumlah stock</label>
                        <input type="text" class="form-control" id="nama" name="stock" value="<?= $Obat['stock']; ?>">
                        <label for="username" class="form-label">Tanggal Expired</label>
                        <input type="date" class="form-control" id="nama" name="tanggal_expired" value="<?= $Obat['tanggal_expired']; ?>">
                        <button type="submit" class="btn btn-primary mt-4">Simpan</button>
                </form>


            </div>
    </section>
</div>