<?php

class M_obat extends CI_Model
{

    public function getAllObat()
    {
        return $this->db->get('obat')->result_array();
    }

    public function insert()
    {
        $data = [
            "nama_obat" => $this->input->post('nama_obat', true),
            "id_jenis_obat" => $this->input->post('id_jenis_obat', true),
            "satuan" => $this->input->post('satuan', true),
            "harga" => $this->input->post('harga', true),
            "stock" => $this->input->post('stock', true),
            "tanggal_expired" => $this->input->post('tanggal_expired', true),
        ];
        $this->db->insert('obat', $data);
    }

    public function getObatById($id_obat)
    {
        return $this->db->get_where('obat', ['id_obat' => $id_obat])->row_array();
    }

    public function update()
    {
        $data = [
            "nama_obat" => $this->input->post('nama_obat', true),
            "satuan" => $this->input->post('satuan', true),
            "harga" => $this->input->post('harga', true),
            "stock" => $this->input->post('stock', true),
            "tanggal_expired" => $this->input->post('tanggal_expired', true),
        ];
        $this->db->where('id_obat', $this->input->post('id_obat'));
        $this->db->update('obat', $data);
    }

    public function delet($id_obat)
    {
        $this->db->where('id_obat', $id_obat);
        $this->db->delete('obat');
    }
}
