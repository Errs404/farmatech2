<?php

class M_user extends CI_Model
{
    public function getAllUser()
    {
        return $this->db->get('user')->result_array();
    }
    public function usercount()
    {
        return $this->db->get('user')->num_rows();
    }

    public function getUserById($id_user)
    {
        return $this->db->get_where('user', ['id_user' => $id_user])->row_array();
    }

    public function update()
    {
        $data = [
            "fullname" => $this->input->post('fullname', true),
            "username" => $this->input->post('username', true),
            "is_active" => $this->input->post('is_active', true),
        ];
        $this->db->where('id_user', $this->input->post('id_user'));
        $this->db->update('user', $data);
    }

    public function delet($id_user)
    {
        $this->db->where('id_user', $id_user);
        $this->db->delete('user');
    }

    public function getUser($username)
    {
        return $this->db->get_where('user', ['username' => $username])->row_array();
    }
}
