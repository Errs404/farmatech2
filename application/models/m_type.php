<?php

class M_type extends CI_Model
{

    public function getAllJenisObat()
    {
        return $this->db->get('jenis_obat')->result_array();
    }

    public function insert()
    {
        $data = [
            "nama_jenis_obat" => $this->input->post('nama_jenis_obat', true),
        ];
        $this->db->insert('jenis_obat', $data);
    }

    public function typecount()
    {
        return $this->db->get('jenis_obat')->num_rows();
    }

    public function getJenisObatById($id_jenis_obat)
    {
        return $this->db->get_where('jenis_obat', ['id_jenis_obat' => $id_jenis_obat])->row_array();
    }

    public function update()
    {
        $data = [
            "nama_jenis_obat" => $this->input->post('nama_jenis_obat', true),
        ];
        $this->db->where('id_jenis_obat', $this->input->post('id_jenis_obat'));
        $this->db->update('jenis_obat', $data);
    }

    public function delet($id_jenis_obat)
    {
        $this->db->where('id_jenis_obat', $id_jenis_obat);
        $this->db->delete('jenis_obat');
    }
}
