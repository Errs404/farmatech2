<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user');
        $this->load->model('M_obat');
        $this->load->model('M_type');
    }

    public function index()
    {
        $data['usercount'] = $this->M_user->usercount();
        // $data2['typecount'] = $this->M_type->typecount();
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('dashboard', $data);
        // $this->load->view('dashboard', $data2);
        $this->load->view('template/footer');
    }
}
