<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Type_obat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_obat');
        $this->load->model('M_type');
    }
    public function index()
    {
        $data['type'] = $this->M_type->getAllJenisObat();
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('type_obat', $data);
        $this->load->view('template/footer');
    }

    public function edit_type($id_user)
    {

        $data['Type'] = $this->M_type->getJenisObatById($id_user);
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('edit_type', $data);
        $this->load->view('template/footer');
    }

    public function edit_Mdata()
    {
        $this->M_type->update();
        redirect('type_obat');
    }

    public function delete($id_user)
    {
        $this->M_type->delet($id_user);
        redirect('type_obat');
    }

    public function add_type()
	{
		$this->load->view('template/header');
        $this->load->view('template/sidebar');
		$this->load->view('add_type');
        $this->load->view('template/footer');
	}

    public function insert()
    {
        $this->M_type->insert();
        redirect('type_obat');
    }
}
