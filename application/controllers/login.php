<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function index()
    {
        $this->load->view('login');
    }

    public function validate()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $cek_user = $this->db->get_where('user', ['username' => $email])->row_array();
        $fullname = $cek_user['fullname'];

        if (empty($email)) {
            echo "<script type='text/javascript'>
    alert('Email Tidak Bolleh Kosong!');
    </script>";

            $this->load->view('login');
        } elseif (empty($password)) {
            echo "<script>
        alert('Password Tidak Bolleh Kosong!');
    </script>";
            $this->load->view('login');
        } elseif ($cek_user) {
            if (password_verify($password, $cek_user['password'])) {

                if ($cek_user['is_active'] == "0") {
                    echo "<script type='text/javascript'>
            alert('Maaf Akun anda beum diaktifkan oleh admmin,silahkan menunggu konfirmasi');
            </script>";
                    $this->load->view('login');
                } else {
?>
                    <script type='text/javascript'>
                        var pushname = '<?= $fullname; ?>';
                        alert('Login Telah Berhasal\nSelamat datang ' + pushname);
                    </script>;
<?php
                    $this->load->view('template/header');
                    $this->load->view('template/sidebar');
                    $this->load->view('dashboard');
                    $this->load->view('template/footer');
                }
                // echo $_SESSION['fullname'];
            } else {
                echo "<script>alert('Maaf, Login Gagal, Password anda tidak sesuai!');</script>";
                $this->load->view('login');
            }
        } else {
            echo "<script>alert('Maaf, Login Gagal, Username anda tidak terdaftar!');
    </script>";
            $this->load->view('login');
        }
    }
}
