<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Obat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_obat');
        $this->load->model('M_type');
    }
    public function index()
    {
        $data['Obat'] = $this->M_obat->getAllObat();
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('obat', $data);
        $this->load->view('template/footer');
    }

    public function edit_obat($id_user)
    {

        $data['Obat'] = $this->M_obat->getObatById($id_user);
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('edit_obat', $data);
        $this->load->view('template/footer');
    }

    public function edit_Mdata()
    {
        $this->M_obat->update();
        redirect('Obat');
    }

    public function delete($id_user)
    {
        $this->M_obat->delet($id_user);
        redirect('Obat');
    }

    public function add_obat()
    {
        $data['JenisObat'] = $this->M_type->getAllJenisObat();
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('add_obat', $data);
        $this->load->view('template/footer');
    }

    public function insert()
    {
        $this->M_obat->insert();
        redirect('Obat');
    }
}
