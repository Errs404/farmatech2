<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user');
    }
    
    public function usercount() 
    {

    }

    public function index()
    {
        $data['User'] = $this->M_user->getAllUser();
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('user', $data);
        $this->load->view('template/footer');
    }

    public function logout()
    {
        $this->session->unset_userdata['username'];
        $this->session->unset_userdata['fullname'];
        return redirect('Login/index');
    }

    public function edit_user($id_user)
    {

        $data['User'] = $this->M_user->getUserById($id_user);
        $this->load->view('template/header');
        $this->load->view('edit_user', $data);
        $this->load->view('template/footer');
    }

    public function edit_Mdata()
    {
        $this->M_user->update();
        redirect('User');
    }

    public function delete($id_user)
    {
        $this->M_user->delet($id_user);
        redirect('User');
    }
}
