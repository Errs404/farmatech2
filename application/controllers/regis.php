<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Regis extends CI_Controller
{
	public function index()
	{
		$this->load->view('register');
	}

	public function action()
	{
		$email = $this->input->post('email');
		$pass1 = $this->input->post('password');
		$pass2 = $this->input->post('password2');
		$cek_user = $this->db->get_where('user', ['username' => $email])->row_array();
		if ($cek_user) {
			echo "<script>alert('Akun anda telah terdaftar silahkan login!');</script>";
			$this->load->view('login');
		} else
		if ($pass1 == $pass2) {
			if ($this->input->post('terms') != '') {
				$enc = password_hash($pass1, PASSWORD_DEFAULT);
				$data = [
					"fullname" => $this->input->post('fullname', true),
					"username" => $this->input->post('email', true),
					"password" => $enc,
					"is_active" => "0",
				];
				$this->db->insert('user', $data);
				echo "<script>alert('Selamat Anda telah berhasil mendaftar, silahkan tunggu konfirmasi admin!');</script>";
				$this->load->view('login');
			} else {
				echo "<script>alert('Maaf, Mohon Stujui Syart Dan Kebajikan Kami');</script>";
				$this->load->view('register');
			}
		} else {
			echo "<script>alert('Maaf, Mohon Cek Password Lagi!');</script>";
			$this->load->view('register');
		}
	}
}
